# Generate QR Code from a URL

This project thanks [That Data Bloke](https://medium.com/@b.arindom) for his
[Generate QRCode With Python In 5
lines](https://towardsdatascience.com/generate-qrcode-with-python-in-5-lines-42eda283f325)
article that inspired this project:

## Example

To create a QR code for a youtube video:

```bash
./url2qr.py --output images/youtube.png https://www.youtube.com/watch?v=V4ui6APsBkc
```

Which will generate something like this:

![YouTube: GitHub Copilot Python Demo 14 March 2023](images/youtube.png "QR code for YouTube video"){width=300}

## References

* [python-qrcode](https://pypi.org/project/python-qrcode/) [homepage](https://github.com/lincolnloop/python-qrcode)
* [QR code (wikipedia)](https://en.wikipedia.org/wiki/QR_code)
