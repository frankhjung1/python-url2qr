#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Generate a QR code from a URL. """

import argparse
import os
import sys

import qrcode

#
# MAIN
#
if __name__ == "__main__":
    __version__ = "0.3.0"

    # options
    parser = argparse.ArgumentParser(
        prog=os.path.basename(sys.argv[0]),
        usage="%(prog)s [options]",
        description="generate a QR code from a URL",
        epilog="© 2023 Frank H Jung, frankhjung at linux.com",
    )
    parser.add_argument(
        "-o", "--output", help="write PNG QR code to a file", required=False
    )
    parser.add_argument("--version", action="version", version=__version__)
    # positional arguments
    parser.add_argument("url", metavar="url", type=str, help="URL to encode")

    # process command line arguments
    args = parser.parse_args()

    # creating an instance of qrcode
    qr = qrcode.QRCode(version=None)
    qr.add_data(args.url)
    qr.make(fit=True)
    # generate and save qrcode image to a file
    QRCODE = qr.make_image(fill="black", back_color="white")
    if args.output:
        QRCODE.save(args.output)  # save to file
    else:
        QRCODE.show()  # display QR code
