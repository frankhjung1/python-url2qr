#!/usr/bin/env make

.PHONY: all check clean help run tags test version

.DEFAULT_GOAL := default

CTAGS	:= $(shell which ctags)
PROJECT	:= url2qr.py

default: check

all:	default run

help:
	@echo
	@echo "Default goal: ${.DEFAULT_GOAL}"
	@echo "  all:   check cover run test doc dist"
	@echo "  check: check style and lint code"
	@echo "  run:   run against test data"
	@echo "  clean: delete all generated files"
	@echo
	@echo "Initialise virtual environment (.venv) with:"
	@echo
	@echo "pip3 install -U virtualenv; python3 -m virtualenv .venv; source .venv/bin/activate; pip3 install -Ur requirements.txt"
	@echo
	@echo "Start virtual environment (.venv) with:"
	@echo
	@echo "source .venv/bin/activate"
	@echo
	@echo "Deactivate with:"
	@echo
	@echo "deactivate"
	@echo

check:	tags style lint

tags:
ifdef CTAGS
	# build ctags for vim
	ctags --recurse -o tags $(PROJECT)
endif

style:
	# sort imports
	isort --line-length 79 $(PROJECT)
	# format code to googles style
	black --line-length 79 -q $(PROJECT)

lint:
	# check with flake8
	flake8 $(PROJECT)
	# check with pylint
	pylint $(PROJECT)

run:	version
	./${PROJECT} --help
	./${PROJECT} --size=10 --output images/youtube.png https://www.youtube.com/watch?v=V4ui6APsBkc

version:
	./$(PROJECT) --version

clean:
	$(RM) -rf __pycache__ **/__pycache__ .pytest_cache/
	$(RM) -v *.pyc *.pyo *.py,cover
	$(RM) -v **/*.pyc **/*.pyo **/*.py,cover

cleanall: clean
	$(RM) -rf .venv
